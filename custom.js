document.querySelector('#playButton').addEventListener('click', play, true);

let dx = 0;
let dy = 0;

function play() {
    let playerOne = document.querySelector('.playerOne');
    let playerTwo = document.querySelector('.playerTwo');
    let play = document.querySelector('.btn-play');

    if (verify(play) === true) {
        playerOne.style.display = "block";
        playerOne.setAttribute('data-attribute', '1');
        playerTwo.style.display = "block";
        playerTwo.setAttribute('data-attribute', '2');
        play.style.display = "none";
    }

    if(playerOne.getAttribute('data-attribute') === '1') {
        document.addEventListener("keydown", move, true);
    }
}

function verify(button) {
    let playerOne = document.querySelector('#playerOneName');
    let playerTwo = document.querySelector('#playerTwoName');
    let errorOne = document.querySelector('.error-one');
    let errorTwo = document.querySelector('.error-two');
    let names = document.querySelector('.score');
    if (playerOne.value <= 3) {
        errorOne.innerText = playerOne.getAttribute('placeholder') + ' required';

    } else {
        errorOne.innerText = '';
    }

    if (playerTwo.value <= 3) {
        errorTwo.innerText = playerTwo.getAttribute('placeholder') + ' required';
    } else {
        errorTwo.innerText = '';
    }

    if ((playerOne.value <= 3) || (playerTwo.value <= 3)) {
        return false;
    } else {
        button.style.display = "none";
        let playerOneElement = document.createElement('p');
        let playerOneName = document.createTextNode(playerOne.value);
        playerOneElement.appendChild(playerOneName);
        names.appendChild(playerOneElement);
        let playerTwoElement = document.createElement('p');
        let playerTwoName = document.createTextNode(playerTwo.value);
        playerTwoElement.appendChild(playerTwoName);
        names.appendChild(playerTwoElement);
        return true;
    }
}

function move(e) {
    let element = document.querySelector('.playerOne');
    let container = document.querySelector('.container');
    let width = container.offsetWidth;
    let height = container.offsetHeight;
    let kc = e.keyCode;

    // left arrow	37
    // up arrow	38
    // right arrow	39
    // down arrow	40

    let Keys = {
        up: false,
        down: false,
        left: false,
        right: false
    }

    if (kc === 37) {
        Keys.left = true;
    }

    if (Keys.left) {
        dx -= 10;
        if (dx >= 0) {
            element.style.left = dx + 'px';
            Keys.left = false;
        } else {
            dx = 0;
            alert('Nope!');
            Keys.up = false;
        }
    }

    if (kc === 39) {
        Keys.right = true;
    }

    if (Keys.right) {
        dx += 10;
        if (dx <= (width - 57)) {
            element.style.left = dx + 'px';
            Keys.right = false;
        } else {
            dx = width - 57;
            alert('Nope!');
            Keys.up = false;
        }
    }
    console.log('dx = ' + dx);
    console.log('container = ' + width);

    if (kc === 38) {
        Keys.up = true;
    }

    if (Keys.up) {
        dy -= 10;
        if (dy >= 0) {
            element.style.top = dy + 'px';
            Keys.up = false;
        } else {
            dy = 0;
            alert('You have gone too far!');
            Keys.up = false;
        }
    }

    if (kc === 40) {
        Keys.down = true;
    }

    if (Keys.down) {
        dy += 10;
        if (dy <= 450) {
            element.style.top = dy + 'px';
            Keys.down = false;
        } else {
            dy = 450;
            alert('Where dafuq you want to go?');
            Keys.up = false;
        }
    }
}
